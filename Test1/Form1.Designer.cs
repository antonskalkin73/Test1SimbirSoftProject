﻿namespace Test1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxAddressText = new System.Windows.Forms.TextBox();
            this.textBoxAddressDictionary = new System.Windows.Forms.TextBox();
            this.buttonAddressText = new System.Windows.Forms.Button();
            this.buttonAddressDictionary = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonWork = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxAddressText
            // 
            this.textBoxAddressText.Location = new System.Drawing.Point(77, 13);
            this.textBoxAddressText.Name = "textBoxAddressText";
            this.textBoxAddressText.ReadOnly = true;
            this.textBoxAddressText.Size = new System.Drawing.Size(444, 20);
            this.textBoxAddressText.TabIndex = 0;
            // 
            // textBoxAddressDictionary
            // 
            this.textBoxAddressDictionary.Location = new System.Drawing.Point(77, 48);
            this.textBoxAddressDictionary.Name = "textBoxAddressDictionary";
            this.textBoxAddressDictionary.ReadOnly = true;
            this.textBoxAddressDictionary.Size = new System.Drawing.Size(444, 20);
            this.textBoxAddressDictionary.TabIndex = 1;
            // 
            // buttonAddressText
            // 
            this.buttonAddressText.Location = new System.Drawing.Point(545, 12);
            this.buttonAddressText.Name = "buttonAddressText";
            this.buttonAddressText.Size = new System.Drawing.Size(73, 20);
            this.buttonAddressText.TabIndex = 2;
            this.buttonAddressText.Text = "Open...";
            this.buttonAddressText.UseVisualStyleBackColor = true;
            this.buttonAddressText.Click += new System.EventHandler(this.buttonAddressText_Click);
            // 
            // buttonAddressDictionary
            // 
            this.buttonAddressDictionary.Location = new System.Drawing.Point(545, 48);
            this.buttonAddressDictionary.Name = "buttonAddressDictionary";
            this.buttonAddressDictionary.Size = new System.Drawing.Size(73, 20);
            this.buttonAddressDictionary.TabIndex = 4;
            this.buttonAddressDictionary.Text = "Open...";
            this.buttonAddressDictionary.UseVisualStyleBackColor = true;
            this.buttonAddressDictionary.Click += new System.EventHandler(this.buttonAddressDictionary_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Dictionary";
            // 
            // buttonWork
            // 
            this.buttonWork.Location = new System.Drawing.Point(15, 85);
            this.buttonWork.Name = "buttonWork";
            this.buttonWork.Size = new System.Drawing.Size(603, 35);
            this.buttonWork.TabIndex = 4;
            this.buttonWork.Text = "WORK!!!";
            this.buttonWork.UseVisualStyleBackColor = true;
            this.buttonWork.Click += new System.EventHandler(this.buttonWork_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 134);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonWork);
            this.Controls.Add(this.buttonAddressDictionary);
            this.Controls.Add(this.buttonAddressText);
            this.Controls.Add(this.textBoxAddressDictionary);
            this.Controls.Add(this.textBoxAddressText);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxAddressText;
        private System.Windows.Forms.TextBox textBoxAddressDictionary;
        private System.Windows.Forms.Button buttonAddressText;
        private System.Windows.Forms.Button buttonAddressDictionary;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonWork;
    }
}

