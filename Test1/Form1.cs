﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void buttonAddressText_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog.OpenFile() != null)
                    {
                        textBoxAddressText.Text = openFileDialog.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void buttonAddressDictionary_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog.OpenFile() != null)
                    {
                        textBoxAddressDictionary.Text = openFileDialog.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void buttonWork_Click(object sender, EventArgs e)
        {
            int countMaxLine = 5000; //Максимальное кол-во строк файла
            int countLine = 0;
            int countFile = 0; // Номер записываемоего файла
            string addressWriteFile;

            try
            {
                HashSet<string> dictionary = getDictionaryToHashSet();
                if(dictionary == null)
                {
                    return;
                }
                addressWriteFile = textBoxAddressText.Text.Replace(".txt", ".html");
                StreamWriter streamWriter = new StreamWriter(addressWriteFile);
                streamWriter.WriteLine("<!DOCTYPE html> <html> <head> <title> Автоматически созданный документ </title> </head> <body> ");
                
                using (StreamReader streamReader = new StreamReader(textBoxAddressText.Text))
                {
                    FileInfo fileInfo = new FileInfo(textBoxAddressText.Text);
                    if (fileInfo.Length > 2097152)
                    {
                        throw new Exception("Слишком большой размер файла с текстом");
                    }

                    string line = "";
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (countLine >= countMaxLine && line.Contains(". "))
                        {
                            int indexDelit = line.IndexOf(". ");
                            streamWriter.WriteLine(getHtmlLine(line.Substring(0, indexDelit+1), dictionary));
                            streamWriter.WriteLine("</body> </html>");
                            streamWriter.Close();
                            countLine = 0;
                            Process.Start(addressWriteFile);
                            countFile++;
                            addressWriteFile = textBoxAddressText.Text.Replace(".txt", "-" + countFile + ".html");
                            streamWriter = new StreamWriter(addressWriteFile);
                            streamWriter.WriteLine("<!DOCTYPE html> <html> <head> <title> Автоматически созданный документ </title> </head> <body> ");
                            streamWriter.WriteLine(getHtmlLine(line.Substring(indexDelit + 1), dictionary) + "<br>");
                        }
                        else
                        {
                            streamWriter.WriteLine(getHtmlLine(line, dictionary) + "<br>");
                        }

                        countLine++;
                    }
                }
                streamWriter.WriteLine("</body> </html>");
                streamWriter.Close();
                Process.Start(addressWriteFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Метод возвращает строку с добавленными тегами
        /// </summary>
        /// <param name="line"></param>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        private string getHtmlLine(string line, HashSet<string> dictionary)
        {
            string[] arrayWords = line.Split(new[] { ' ', ',', ':', '?', '!', '.', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> listWordUsing = new List<string>();
            for (int i = 0; i < arrayWords.Length; i++)
            {
                if (dictionary.Contains(arrayWords[i].ToLower()))
                {
                    listWordUsing.Add(arrayWords[i]);
                }
            }

            foreach (string word in listWordUsing)
            {
                int indexStartPosition = -1;
                while (true)
                {
                    indexStartPosition = line.IndexOf(word, indexStartPosition > -1 ? indexStartPosition : 0);
                    if (indexStartPosition < 0)
                        break;
                    if (line[indexStartPosition > 0 ? indexStartPosition - 1 : 0] < 64
                        && line[indexStartPosition + word.Length < line.Length - 1 ? indexStartPosition + word.Length : indexStartPosition - 1] < 64)
                    {
                        line = line.Insert(indexStartPosition + word.Length, "</em></strong>");
                        line = line.Insert(indexStartPosition, "<strong><em>");
                        indexStartPosition += 23 + word.Length;
                    }
                    else
                    {
                        indexStartPosition += word.Length;
                    }
                }
            }

            return line;
        }

        /// <summary>
        /// Метод возвращает коллекцию словаря загруженную из файла
        /// </summary>
        /// <returns></returns>
        private HashSet<string> getDictionaryToHashSet()
        {
            HashSet<string> hashSet = new HashSet<string>();
            try
            {
                using (StreamReader streamReader = new StreamReader(textBoxAddressDictionary.Text))
                {
                    FileInfo fileInfo = new FileInfo(textBoxAddressDictionary.Text);
                    if(fileInfo.Length > 2097152)
                    {
                        throw new Exception("Слишком большой размер словаря");
                    }

                    string line = "";
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if(line.Split(' ').Length == 1)
                        {
                            hashSet.Add(line.ToLower());
                        }
                        else
                        {
                            throw new Exception("Неправильная структура словаря");
                        }
                    }
                }
                if(hashSet.Count > 100000)
                {
                    throw new Exception("Слишком большой размер словаря");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            return hashSet;
        }
    }
}
